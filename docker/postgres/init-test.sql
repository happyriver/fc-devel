-- Tests reset their state by dropping the public schema.
grant all privileges on database throat_test to throat;
alter schema public owner to throat;

create role throat_py with login;
alter user throat_py with password 'py';

create role throat_be with login;
alter user throat_be with password 'be';

create role ovarit_auth with login;
alter user ovarit_auth with password 'auth';

create role ovarit_subs with login;
alter user ovarit_subs with password 'subs';
