import json
import os
import time

from aiosmtpd.controller import Controller
import click
from redis import Redis

class CustomHandler:

    def __init__(self, channel):
        self.channel = channel
        if self.channel:
            self.conn = Redis.from_url(os.getenv('REDIS_URL'))

    async def handle_DATA(self, server, session, envelope):
        email = {"from": envelope.mail_from,
                 "to": envelope.rcpt_tos,
                 "content": envelope.content.decode("utf-8")}
        print(f"mail_from={email['from']}")
        print(f"rcpt_tos={email['to']}")
        print(f"content={email['content']}")

        if self.channel:
            self.conn.publish(self.channel, json.dumps(email))

        return '250 OK'


@click.command()
@click.option("--hostname", default="127.0.0.1", help="Address to listen on")
@click.option("--port", default=8025, help="Port to listen on")
@click.option("--channel", default="", help="Redis channel to publish emails to")
def listen(hostname, port, channel):
    handler = CustomHandler(channel)

    try:
        controller = Controller(handler, hostname=hostname, port=port)
        controller.start()
        while True:
            time.sleep(0.01)
    finally:
        controller.stop()


if __name__ == '__main__':
    listen()
