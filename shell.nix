# For use with NixOS, direnv and lorri.
#
# Include the following in your configuration.nix.
#
# virtualisation.docker.rootless = {
#     enable = true;
#     setSocketVariable = true;
# };
#
# If you get errors about the docker daemon not running,
# try:
#
# systemctl --user start docker

let
  pkgs = import <nixpkgs> {};
in pkgs.mkShell {
  buildInputs = with pkgs; [
    docker-compose gnumake stripe-cli
  ];
  shellHook = ''
    export CURRENT_USER=0:0
    export COMPOSE_HTTP_TIMEOUT=120
  '';
}
