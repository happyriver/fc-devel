# Provide a nice prompt if the terminal supports it.
if [ "$TERM" != "dumb" ]; then
  PROMPT_COLOR="1;31m"
  let $UID && PROMPT_COLOR="1;36m"
  PS1="\n\[\033[$PROMPT_COLOR\][\[\e]0;@$CONTAINER_NAME: \w\a\]@$CONTAINER_NAME:\w]\\$\[\033[0m\] "
  if test "$TERM" = "xterm"; then
    PS1="\[\033]2;@$CONTAINER_NAME:\w\007\]$PS1"
  fi
fi
