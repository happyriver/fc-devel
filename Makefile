ifeq ($(CURRENT_USER),)
CURRENT_USER=$(shell id -u):$(shell id -g)
export CURRENT_USER
endif

.PHONY: get-started
.PHONY: up down migrations schema
.PHONY: auth-test graphql-test python-test
.PHONY: shell repl-be repl-fe check-nginx-conf
.PHONY: pull-and-build build translations npm-install backup restore
.PHONY: export import
.PHONY: help

# Show this help
help:
# This shows a one-line comment placed before a target.
	@awk '/^#/{c=substr($$0,3);next}c&&/^[[:alpha:]][[:alnum:]_-]+:/{print substr($$1,1,index($$1,":")),c}1{c=0}' $(MAKEFILE_LIST) | column -s: -t

throat:
	git clone https://gitlab.com/feminist-conspiracy/throat.git
	cp throat/example.config.yaml throat/config.yaml
	cp throat/example.test-config.yaml throat/test-config.yaml
	mkdir -p throat/stor

ovarit-app:
	git clone https://gitlab.com/happyriver/ovarit-app.git
	cp ovarit-app/example.env ovarit-app/.env
	cp ovarit-app/dev-resources/logback-test-example.xml \
	   ovarit-app/dev-resources/logback-test.xml

ovarit-auth:
	git clone https://gitlab.com/feminist-conspiracy/ovarit-auth.git
	touch ovarit-auth/.trigger

ovarit-db:
	git clone https://gitlab.com/feminist-conspiracy/ovarit-db.git
	cp ovarit-db/example.env ovarit-db/.env

ovarit-subscription:
	git clone https://gitlab.com/feminist-conspiracy/ovarit-subscription.git
	cat ovarit-subscription/.env | grep -v DATABASE > ovarit-subscription/.env.development
	touch ovarit-subscription/.trigger

# Pull newer versions of docker base images, and build all the containers
pull-and-build:
	docker-compose pull
	# docker-compose pull doesn't pull images named in Dockerfiles,
	# but this does:
	docker-compose build --no-cache --pull

# Build the docker containers
build: | throat ovarit-app ovarit-auth ovarit-db ovarit-subscription
	docker-compose build

DC_RUN = docker-compose run --rm
COMPILE_TRANSLATIONS = pybabel compile -d app/translations
NPM_INSTALL = bash -c "npm install --no-save && npm run build"
PY_MIGRATIONS = python throat.py migration apply
PY_MODMAIL_MIGRATIONS = ${PY_MIGRATIONS} --dirname modmail_migrations
DBMATE = docker-compose run --rm dbmate

# Start from scratch.  Run this first
get-started: pull-and-build
	${DC_RUN} --name=throat-translations -w /throat throat \
	    ${COMPILE_TRANSLATIONS}
	${DC_RUN} --name=npm -w /throat webpack \
            ${NPM_INSTALL}
	# Running translations in throat will bring up its
	# dependencies, db and db-test, making it possible to run the
	# script that creates user roles in the database. Roles must
	# be created before migrations are run.
	docker-compose exec db create-roles
	${DC_RUN} --name=migrations -w /throat \
	    -e DATABASE_USER=throat -e DATABASE_PASSWORD=dev throat \
	    ${PY_MIGRATIONS}
	${DC_RUN} --name=modmail-migrations -w /throat \
	    -e DATABASE_USER=throat -e DATABASE_PASSWORD=dev throat \
	    ${PY_MODMAIL_MIGRATIONS}
	${DBMATE} migrate
	docker-compose up --remove-orphans -d

# Recompile translations
translations:
	${DC_RUN} --name=throat-translations -w /throat throat \
	    ${COMPILE_TRANSLATIONS}

# Re-install node packages and build the web assets
npm-install:
	${DC_RUN} --name=npm -w /throat webpack \
            ${NPM_INSTALL}

# Bring up the servers and their supporting services
up:
	docker-compose up --remove-orphans -d

# Shut down all containers
down:
	docker-compose down --remove-orphans

# Get a shell in the Python container
shell:
	${DC_RUN} throat bash

# Run the database migrations
migrations:
# This runs them up to the most current state.  If you need to roll
# them back or only do partial migrations, use the command line.
# Ensure roles are created first, in case migrations are run on
# a database created before the roles were added.
	docker-compose exec db create-roles
	${DC_RUN} --name=migrations -w /throat  \
	    -e DATABASE_USER=throat -e DATABASE_PASSWORD=dev throat \
	    ${PY_MIGRATIONS}
	${DC_RUN} --name=modmail-migrations -w /throat \
	    -e DATABASE_USER=throat -e DATABASE_PASSWORD=dev throat \
	    ${PY_MODMAIL_MIGRATIONS}
	${DBMATE} migrate

# Update the database schema in ovarit-db/schema.sql
schema:
	docker-compose exec -T db-test psql -q -c \
	  "DROP SCHEMA public CASCADE; CREATE SCHEMA public; DROP SCHEMA IF EXISTS proletarian CASCADE;"
	${DBMATE} -e TEST_DATABASE_URL up
	docker-compose exec -T db-test pg_dump -O --column-inserts throat_test > ovarit-db/schema.sql

# Connect to the Clojure REPL for the GraphQL server
repl-gql:
# Highly recommended: use an editor with nREPL support instead,
# and connect it to 127.0.0.1:8778.
	docker-compose exec ovarit-app-gql bin/nrepl

# Run the Clojure and Clojurescript REPLs in throat-fe
repl-fe:
	@echo "--> Use (shadow/repl :app) to connect to the browser REPL"
	@echo "--> Use (ns user) to work with user.clj utilities"
	@echo
	docker-compose exec ovarit-app-fe npx shadow-cljs clj-repl

# Check the nginx configuration
check-nginx-conf:
	${DC_RUN} nginx nginx -c /etc/nginx/nginx.conf -t

# Run the Python tests
python-test:
	${DC_RUN} throat python -m pytest

# Run the GraphQL server tests
graphql-test:
	${DC_RUN} ovarit-app-gql bin/kaocha --focus :integration

# Run the ovarit-auth integration tests
auth-test:
	docker-compose exec -T db-test psql -q -b \
	  -c "DROP SCHEMA public CASCADE; CREATE SCHEMA public; DROP SCHEMA IF EXISTS proletarian CASCADE;"
	${DBMATE} -e TEST_DATABASE_URL up
	docker-compose run --rm \
	  -e DATABASE_HOST=db-test -e DATABASE_NAME=throat_test \
	  -e RUST_BACKTRACE=0 \
	  ovarit-auth cargo test --features integration

BACKUPFILE = $(shell date +db-%Y%m%d-%H%M%S)

# Backup the database to the backup subdirectory
backup:
	docker-compose pause
	docker-compose run --rm -v ${PWD}/backup:/backup db \
	    tar cvf /backup/${BACKUPFILE}.tar /var/lib/postgresql/data
	docker-compose unpause

# Restore a database backup from the backup subdirectory using "FROM=file make restore"
restore:
	@[ "${FROM}" ] || ( echo ">> FROM is not set"; exit 1 )
	@[ -f "backup/${FROM}" ] || ( echo ">> backup/${FROM} does not exist"; exit 1 )
	docker-compose stop
	docker-compose run --rm -v ${PWD}/backup:/backup db \
	    tar xvf /backup/${FROM}
	docker-compose start

# Export the database to the backup subdirectory
export:
	docker-compose exec db \
            pg_dump -c --if-exists > backup/${BACKUPFILE}.sql

import:
	# not quite done, need to figure out how to do this without
	# needing psql on the host system
	@[ "${FROM}" ] || ( echo ">> FROM is not set"; exit 1 )
	@[ -f "backup/${FROM}" ] || ( echo ">> backup/${FROM} does not exist"; exit 1 )
	docker-compose down --remove-orphans
	docker volume rm fc-devel_db-data
	docker-compose up --remove-orphans -d
	docker-compose exec db create-roles
	psql -h 127.0.0.1 -p 5432 -U throat < "backup/${FROM}"
