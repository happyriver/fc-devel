# Getting started

These instructions are intended to get you set up to develop on
the Feminist Conspiracy's version of Throat, in minutes!

# Dependencies

If you don't have docker installed, follow the instructions here:

[https://www.docker.com/get-started](https://www.docker.com/get-started)

If your installation of docker did not include `docker-compose`,
follow the directions at
[https://docs.docker.com/compose/install/](https://docs.docker.com/compose/install/)
to install it.

Install GNU make, if you don't have it already.  If you are using
Ubuntu, use `sudo apt-get install make` to install it.  Otherwise,
refer to the directions for your package manager.

The other thing you'll need is git (`sudo apt-get install git`).

## Rootless docker

The default instructions for installing docker will install and run it with
root permissions.  If you find that your user account is part of the `docker`
group, this is how your docker is installed.  There are some unavoidable
security vulnerabilities when docker is run this way.

The alternative is [Rootless mode](https://docs.docker.com/engine/security/rootless/). 

Since docker installed with root permissions is the most common, this
project assumes that's what you have.  If you are running docker in
rootless mode, you will need to set the environment variable
`CURRENT_USER` to `0:0` before running any of the `make` commands
described below, or you will see errors about file permissions.

# Setting up your development directory

Start by cloning this repo, and using its directory as your
new base of operations.  At a shell prompt:

```
git clone https://gitlab.com/happyriver/fc-devel.git
cd fc-devel
```

The make target `get-started`, which you only need to use once, will
clone the projects that make up the Feminist Conspiracy server, and
set up your initial configuration files:

```
make get-started
```

### What does `make get-started` do?

First, it clones the projects that make up the Feminist Conspiracy
server.  `feminist-conspiracy/throat` is our fork of [the upstream
version of Throat](https://github.com/phuks-co/throat), which is
written in Python using the Flask framework, and which includes a
little vanilla JavaScript for front-end features.  Our fork has a few
extra features, and some terminology changes (circles instead of
subs).

We have implemented several improvements, including a moderator mail
system, in a separate server,
[https://gitlab.com/happyriver/ovarit-app](https://gitlab.com/happyriver/ovarit-app). which
is written in Clojure and ClojureScript and implements a GraphQL
interface to the same database used by the `throat` server.

There is also a service which implements our subscriptions and
payments, using Stripe.  This can be found at 
[https://gitlab.com/feminist-conspiracy/ovarit-subscription](https://gitlab.com/feminist-conspiracy/ovarit-subscription)
and is implemented in Rust.

The last service handles user authentication, and can be found at
[https://gitlab.com/feminist-conspiracy/ovarit-auth](https://gitlab.com/feminist-conspiracy/ovarit-auth)
and is also implemented in Rust.

The database schema and migrations have their own git repo, found at
[https://gitlab.com/happyriver/ovarit-db](https://gitlab.com/happyriver/ovarit-db).

The next thing `get-started` does is to copy the supplied sample
configuration files into the places where the servers look for their
configuration.  Examine the `Makefile` targets `throat`, `ovarit-app`,
`ovarit-db` and `ovarit-subscription` to see what they are.
Optionally, you can look at these files and change things, but you
don't have to.  Some of the values in them will be overruled by
`docker-compose.yml`.

Next, `get-started` will build docker containers for all the services
used by Throat, and then do some preparatory work before the server
can be started: compiling translation files and web assets, and
running database migrations.  Once that is done, it will start
the development servers.

# You're good to go!

To access your new development site, visit
[http://localhost:5000](http://localhost:5000) in your web browser.

Next, go edit the file `throat/app/html/shared/layout.html` and add a
smiley `:)` at the end of the line with the Sheila Jeffreys quote, and
then go refresh your browser.  You're in!

If refreshing stops working, that usually means whatever you changed
caused an error and stopped a service.  Use the logs command,
described below, to figure out what went wrong, and then
`docker-compose restart throat`, or whichever service failed, to get
it started again.

# Other useful stuff 

## Stopping the containers

`make down` will stop all the running services, and clean up.

## Starting the containers

If you used `make down`, to stop the servers and their dependencies,
then `make up` will start them up again without rebuilding everything.

## Updating

You can update any of the sub-projects using `git pull` inside the
appropriate subdirectory. Depending on what was changed, you might
need to restart containers or rebuild them.

You can fetch newer versions of the base Docker images and rebuild
everything by using `make down` followed by `make get-started`.

## Logs

Once the containers are up, you can use `docker-compose logs db` to
see the logs for the database container, or replace `db` with one of
`redis`, `nginx`, `webpack`, `throat`, `ovarit-app`, or `ovarit-auth`
to see the other parts of the project.

A useful technique is to dedicate a terminal window to `docker-compose
logs -f --tail=100` which will show all the logs mixed together, and
may be helpful for diagnosing which part of the project is responsible
for an error condition.

The default development configuration for the GraphQL server outputs
errors and warnings to the docker logs and much, much more
comprehensive logs to the `ovarit-app/logs` directory.

## Restarting things

As described above, you can restart any service using `docker-compose
restart`.  If you edit `throat/config.yaml`, you will need to restart
`throat` to see the changes.

The projects are all set up to automatically reload and restart when
you edit a file.

Sometimes `nginx`, which coordinates the other containers, drops its
connection to one of them after a restart.  Symptoms of this are long
delays and 502 errors in the browser.  `docker-compose restart nginx`
should restore it to normal operation.

## Command line access

`make shell` will give you a shell prompt in the Python environment.

After you make a user using the web interface, you'll probably want to
promote that user to an admin using: `python throat.py admin add
{{username}}` at that shell prompt.

Check out the `throat/cli` directory for more useful things to do with
`python throat.py`.

Running `flask shell` at the shell prompt will give you a Flask shell,
from which you can call functions to access the database, or try out
bits of Python.

`make repl-gql` and `make repl-fe` will give you Clojure REPLs in the
running code (the latter requires you to have a `ovarit-app` page
loaded in your browser).  Instead of using the REPLs in the terminal,
using an editor with nREPL support is a much better way to interact
with them.

## Running tests

`make python-test` will run the Python server's tests.  To run the
GraphQL server's tests, use `make graphql-test` or an editor with
nREPL integration.  The ClojureScript tests will run if you load
http://localhost:8290/ in your browser. `make auth-test` will run the
login system's integration tests.

## Running the database migrations

Database migrations can be run using: `make migrations`.  If you need
to roll back a migration, use:

```
docker-compose run --rm dbmate rollback
```

For other dbmate commands, see
[`dbmate`](https://github.com/amacneil/dbmate).

### Updating the database schema

An automatically generated database schema can be found in
`ovarit-db/schema.sql`, and updated after you create a new migration
by `make schema`.  This schema is used by the tests for the GraphQL
server and `ovarit-auth`.  In order for the GraphQL tests to find it,
it currently must be manually copied into the
`ovarit-app/dev-resources/sql` directory.

## Command-line access to the database

When the project is running, get command line superuser access to your
database using `docker-compose exec db psql`.

## Using the docker-compose command directly

The container definitions use the environment variable `CURRENT_USER`.
This is set up automatically in the `Makefile` but if you run
`docker-compose` commands at your shell prompt, many will complain if
this variable is not set.  Use `source setup.sh` to set this variable
in your shell, or check out [`direnv`](https://direnv.net) which is a
convenient way to handle the situation.

## Get help

Use `make help` to see a list of the `make` targets you can use and
what they do.

# Port conflicts

This project uses these ports: 5000, 5003, 5432, 5555, 8260, 8280,
8290, 8777, 8778, 8787, 8888, 9000, and 9630.

If something else on your machine is using those ports already, you
will have a conflict.  Most ports can be changed in
`docker-compose.yml`, except for 9360 which is used for live reloading
by the ClojureScript development server.

## Stripe notes

If you are working on payments and need full Stripe functionality 
in your development environment, you will need to use the Stripe
command-line utility to forward webhooks.

 `stripe-cli` is included in `shell.nix`, or follow Stripe's
instructions to install it if you are using a different package
manager.

To forward Stripe's webhooks:

```
stripe listen -f http://localhost:5003/subscription/stripe_webhook
```

This will print a signing secret, which you should assign to the environment variable
`STRIPE_SIGNING_SECRET` in `ovarit-subscription/.env.development`.

To see your Stripe logs:

```
stripe logs tail
```

## Windows

The above instructions for getting started have been tested on Ubuntu
20.04 running in the WSL.


