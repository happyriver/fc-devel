# Use "source setup.sh" to add CURRENT_USER to your shell variables
# before running docker-compose directly.

CURRENT_USER=$(shell id -u):$(shell id -g)
export CURRENT_USER
